package com.gebauer.modernartui;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.SeekBar;
import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SeekBar seekBar =  (SeekBar)findViewById(R.id.seekBar);

        final ImageView imageViewGray = (ImageView)findViewById(R.id.imageView4);
        imageViewGray.setBackgroundColor(Color.rgb(128,128,128));

        final ArrayList<ImageView> imageViews = new ArrayList<ImageView>();
        imageViews.add((ImageView)findViewById(R.id.imageView1));
        imageViews.add((ImageView)findViewById(R.id.imageView2));
        imageViews.add((ImageView)findViewById(R.id.imageView3));
        imageViews.add((ImageView)findViewById(R.id.imageView5));

        final float hsvColors[][] = initWithRandomColors(imageViews);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean boo) {

                for (int i = 0; i < imageViews.size(); i++)
                {
                    ImageView imageView = imageViews.get(i);

                    Drawable background = imageView.getBackground();
                    if (background instanceof ColorDrawable)
                    {
                        int newColor = Color.HSVToColor(new float[]{(hsvColors[i][0] + progress) % 360, hsvColors[i][1], hsvColors[i][2]});
                        ((ColorDrawable) background).setColor(newColor);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            new MyDialogFragment().show(getSupportFragmentManager(), "dialog");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private float[][] initWithRandomColors(ArrayList<ImageView> imageViewList) {

        if (imageViewList == null || imageViewList.size() == 0)
            throw new IllegalArgumentException("Image View List is empty");

        Random random = new Random();

        int count = imageViewList.size();

        float colors[][] = new float[count][3];
        for (int i = 0; i < count; i++)
        {
            colors[i][0] = i * 360/count + random.nextInt(360/count );
            colors[i][1] = 1.0f;
            colors[i][2] = 1.0f;

            imageViewList.get(i).setBackgroundColor( Color.HSVToColor(colors[i]));
        }

        return colors;
    }
}
